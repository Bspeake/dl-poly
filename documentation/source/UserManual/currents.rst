.. _currents:

Currents
========

Introduction
~~~~~~~~~~~~

DL_POLY_5 includes a currents module taking user defined samples
in :math:`{\bf k}`-space from a KPOINTS file, which may be used to calculate 
fundamental quantities for studying the liquid state: density, 
longitudinal and transverse currents, energy-density, and energy currents,
as well as the stress tensor in :math:`{\bf k}`-space :cite:`Balucani1995Dynamics`. 

Beginning in :math:`{\bf k}`-space the density can be written as

.. math:: n({\bf k, t}) = \sum_{i}e^{i{\bf k}\cdot {\bf r}_{i}(t)},
      :label: k-density

where :math:`{\bf r}_{i}(t)` is the position of atom :math:`i` at
time :math:`t`. Which is obtained as the Fourier transform of the
scalar density field

.. math:: n({\bf R}, t) = \sum_{i}\delta({\bf R}-{\bf r}_{i}(t)),
    :label: sdf

for dirac delta :math:`\delta`.

For the longitudinal,
:math:`j_{L}({\bf k}, t)`, and transverse,
:math:`j_{T}({\bf k}, t)`, components of the current we
write

.. math:: j_{L}({\bf k}, t) = \sum_{i}({\bf v}_{i}(t)\cdot\hat{{\bf k}})\hat{{\bf k}}e^{i{\bf k}\cdot {\bf r}_{i}(t)},
    :label: longitudinal_cur

and

.. math:: j_{T}({\bf k}, t) = \sum_{i}[{\bf v}_{i}(t)-({\bf v}_{i}(t)\cdot\hat{{\bf k}})\hat{{\bf k}}]e^{i{\bf k}\cdot {\bf r}_{i}(t)}.
    :label: transverse_cur

Where :math:`\hat{\cdot}` is a unit vector, and
:math:`{\bf v}_{i}(t)` is the velocity of atom :math:`i`. Correlations
of the density or currents may be used to obtain the
intermediate scattering function and the dynamic structure factor.

We can also define a :math:`{\bf k}`-dependent energy density and 
stress tensor by

.. math:: e({\bf k}, t) = \frac{1}{2}\sum_{i}\biggl(mv_{i}^{2}(t) + \sum_{j\neq i}U(r_{ij}(t))\biggr)e^{i{\bf k}\cdot {\bf r}_{i}(t)},
    :label: eng_density

and

.. math:: {\bf \sigma}_{\alpha, \beta}({\bf k}) = \sum_{i}\biggl( mv_{i,\alpha}v_{i,\beta}-\frac{1}{2}\sum_{j\neq i}\frac{r_{ij,\alpha}r_{ij,\beta}}{|r_{ij}|^2}P({\bf k}, r_{ij}) \biggr)e^{i{\bf k}\cdot {\bf r}_{i}(t)}
    :label: k-stress

where

.. math:: P_{k}({\bf k}, r_{ij}) = |{\bf r}_{ij}|\frac{\partial U(|{\bf r}_{ij}|)}{\partial r}\frac{1-e^{-i{\bf k}\cdot {\bf r}_{i}}}{i{\bf k}\cdot {\bf r}_{i}}

Finally the energy current, restricting to pairwise interactions, has the form

.. math:: {\bf q}^{a}({\bf k}) = \frac{1}{2} \sum_{i} \biggl[ e_i {\bf v}_{i}^{a} - \frac{1}{2}\sum_{j\neq i}\sum_{b}({\bf v}_{i}^{b}+{\bf v}_{j}^{b})({\bf r}_{ij}^{a}{\bf r}_{ij}^{b}/|{\bf r}_{ij}|^2)P({\bf k}, r_{ij})\biggr]e^{i{\bf k}\cdot {\bf r}_{i}(t)}
    :label: energy_cur

for Cartesian component :math:`a`.

User Control
~~~~~~~~~~~~

In DL_POLY_5 a user may calculate these quantities (density, 
longitudinal and transverse momentum currents, and energy current)
for user supplied samples of :math:`{\bf k}`-space. Currently only two body Van Der Waals has
full support for energy currents and :math:`{\bf k}`-dependent stress.

Input
^^^^^

In order to calculate density and the momentum currents the user
must supply

1. A KPOINTS file with :math:`{\bf k}`-space samples.
2. **currents_calculate On** in the new style CONTROL (see Section :ref:`new-control-file`).
3. For energy currents and :math:`{\bf k}` dependent stress also **energy_stress_currents On** must also be specified.

See :ref:`kpoints-file_sec` for the KPOINTS format.

Performance
^^^^^^^^^^^

Current calculations require reducing statistics across atoms in the system and among processors, as well
as collection of additional data during force calculation. **energy_stress_currents** require additional 
specification due to the increased complexity of force calculations due to Equations :eq:`energy_cur` and :eq:`k-stress`.

The frequency of statistics collection, the number of KPOINTS, and the number of atoms can all be varied to
balance the requirements of statistical accuracy.

Output
^^^^^^

The CURRENTS file will output each current, for each atom type, at each time step
(at rate of the **stats_frequency** steps). If **io_statis_yaml On** is present in CONTROL
the output will be in the form of a YAML file like the snippet below for LiF

::

    %YAML 1.2
    ---
    title: CONFIG generated by ASE
    timesteps:
    - { time:    0.0000000    ,
        density: {
                  Li: [   ...   ],
                  F: [   ...    ]
            },
        longitudinal: {
                  Li: [    ...   ],
                  F: [    ...   ]
            },
        transverse: {
                  Li: [  ...    ],
                  F: [   ...    ]
            },
        energy_density: {
                  Li: [  ...    ],
                  F: [  ...   ]
            }
    }

    - { time:   0.10000000E-02, ...

Otherwise refer to Section :ref:`currents-file_sec` for the plaintext
format.

Performance
^^^^^^^^^^^

For statistics calculation of Equations :eq:`k-density`, :eq:`longitudinal_cur`, :eq:`transverse_cur`, and :eq:`energy_cur` scale with
the number of user supplied KPOINTS and the number of atoms. Note Equation :eq:`energy_cur` requires additional calculations during 
force calculation for each atom and user KPOINTS, this will impact Van Der Waals performance. 